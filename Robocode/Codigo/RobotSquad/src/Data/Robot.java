package Data;

import java.awt.geom.Point2D;
import java.io.Serializable;

public class Robot implements Serializable{
    
    // ----- VARIABLES ----- //
        
        // Position
        private Point2D position;
        
        // Heading
        private double bodyHeading;
        
        // Bearing
        private double bearing;
        
        // Velocity
        private double velocity;
        
        // Energy
        private double robotEnergy;
        
        // Damage taken
        private double stolenEnergy;
            
    // ----- CONSTRUCTORS ----- //
    
    public Robot(double x, double y, double h, double b, double v, double e) {
        this.position = new Point2D.Double(x, y);
        this.bodyHeading = h;
        this.bearing = b;
        this.velocity = v;
        this.robotEnergy = e;
        this.stolenEnergy = 0.0;
    }
    
    public Robot(Robot e) {
        this.position = e.getPosition();
        this.bodyHeading = e.getBodyHeading();
        this.velocity = e.getVelocity();
        this.robotEnergy = e.getRobotEnergy();
        this.stolenEnergy = e.getStolenEnergy();
    }
    
    // ----- GETS ----- //
    
    public Point2D getPosition() {
        return this.position;
    }

    public double getBodyHeading() {
        return this.bodyHeading;
    }

    public double getBearing() {
        return this.bearing;
    }

    public double getVelocity() {
        return this.velocity;
    }

    public double getRobotEnergy() {
        return this.robotEnergy;
    }

    public double getStolenEnergy() {
        return this.stolenEnergy;
    }
    
    // ----- SETS ----- //

    public void setPosition(Point2D p) {
        this.position = new Point2D.Double(p.getX(), p.getY());
    }

    public void setBodyHeading(double h) {
        this.bodyHeading = h;
    }

    public void setBearing(double b) {
        this.bearing = b;
    }

    public void setVelocity(double v) {
        this.velocity = v;
    }

    public void setRobotEnergy(double re) {
        this.robotEnergy = re;
    }

    public void setStolenEnergy(double se) {
        this.stolenEnergy = se;
    }
    
    // ----- CLONE + EQUALS ---- //
    
    @Override
    public Robot clone(){
        return new Robot(this);
    }
    
    @Override
    public boolean equals(Object o) {
        boolean result;
        
        // Check class
        if((o == null) || (this.getClass() != o.getClass())) {
            result = false;
        } 
        // Check position
        else {
            // Check if error is defined
            Robot enemy = (Robot) o;
            result = this.position.equals(enemy.getPosition());
        }

        return result;
    }
    
    // ----- METHODS ----- //
    
    public void makeDamage(double d) {
        this.stolenEnergy += d;
    }
}
