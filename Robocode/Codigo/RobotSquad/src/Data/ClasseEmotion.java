package Data;

import java.awt.Color;
import java.util.ArrayList;

public class ClasseEmotion {
    
    // ----- VARIABLES ----- //
    
        private Measure intensity; // Varies between 0 and 1
        
        // Movement
        /* 
         * Still
         * Linear
         * Circle
         * Random
         */
        private ArrayList<String> robotMovement;
        
        // Shooting method
        /* 
         * Point
         * Linear
         * Circle
         * Random
         */
        private ArrayList<String> robotShooting;
        
        // Target choosing
        /* 
         * First
         * Closest
         * Revenge (Robot who did the most damage to you)
         * Slayer (Robot who has the lest knowned energy value)
         */
        private ArrayList<String> robotTargeting;
        
    // ----- CONSTRUCTORS ----- //
        
    public ClasseEmotion() {
        this.intensity = new Measure("EMOTION", 3);
        
        this.constructMovement();
        this.constructShooting();
        this.constructTarget();
    }
    
    // ----- METHODS ----- //
    
    public Measure getIntensity() {
        return this.intensity;
    }
    
    public ArrayList<String> getMovementsList() {
        return this.robotMovement;
    }
    
    public ArrayList<String> getShootingsList() {
        return this.robotShooting;
    }
    
    public ArrayList<String> getTargetingsList() {
        return this.robotTargeting;
    }
    
    public String getBestMovement() {
        Long l = Math.round(this.intensity.getEfficiency() * 6 - 3);
        int closestIndex = Math.abs(l.intValue());
        return this.robotMovement.get(closestIndex);
    }
    
    public String getBestShooting() {
        Long l = Math.round(this.intensity.getEfficiency() * 6 - 3);
        int closestIndex = Math.abs(l.intValue());
        return this.robotShooting.get(closestIndex);
    }
    
    public String getBestTargeting() {
        Long l = Math.round(this.intensity.getEfficiency() * 6 - 3);
        int closestIndex = Math.abs(l.intValue());
        return this.robotTargeting.get(closestIndex);
    }
    
    public void missShot() {
        // Count received shot for current class
        this.intensity.missShot();
    }
    
    public void receiveShot() {
        // Count received shot for current class
        this.intensity.receiveShot();
    }
    
    public void shootSomeone() {
        // Count hit shot for current class
        this.intensity.shootSomeone();
    }
    
    public int getMood() {
        Long l = Math.round(this.intensity.getEfficiency() * 6 - 3);
        int rangeValue = l.intValue();
        return rangeValue;
    }
    
    public String getMoodName() {
        String result = "NONE";
        
        Long l = Math.round(this.intensity.getEfficiency() * 6 - 3);
        int rangeValue = l.intValue();
        
        switch(rangeValue) {
            case -3:
                result = "RAIVA";
                break;
            case -2:
                result = "TRISTEZA";
                break;
            case -1:
                result = "MEDO";
                break;
            case 0:
                result = "NEUTRO";
                break;
            case 1:
                result = "NORMAL";
                break;
            case 2:
                result = "FELIZ";
                break;
            case 3:
                result = "EXALTADO";
                break;
        }
        
        return result;
    }
    
    public Color getMoodColor(String name) {
        Color color = Color.BLACK;
        
        switch(name) {
            case "RAIVA":
                color = new Color(255, 0, 0);
                break;
            case "TRISTEZA":
                color = new Color(255, 85, 0);
                break;
            case "MEDO":
                color = new Color(255, 170, 0);
                break;
            case "NEUTRO":
                color = new Color(255, 255, 0);
                break;
            case "NORMAL":
                color = new Color(170, 255, 0);
                break;
            case "FELIZ":
                color = new Color(85, 255, 0);
                break;
            case "EXALTADO":
                color = new Color(0, 255, 0);
                break;
        }
        
        return color;
    }
    
    // ----- AUXILIARY METHODS ---- //
    
    private void constructMovement() {
        // Init
        this.robotMovement = new ArrayList<>();
        
        // Add classes
        this.robotMovement.add("STILL");
        this.robotMovement.add("LINEAR");
        this.robotMovement.add("CIRCLE");
        this.robotMovement.add("RANDOM");
    }
    
    private void constructShooting() {
        // Init
        this.robotShooting = new ArrayList<>();
        
        // Add classes
        this.robotShooting.add("POINT");
        this.robotShooting.add("LINEAR");
        this.robotShooting.add("CIRCLE");
        this.robotShooting.add("RANDOM");
    }
    
    private void constructTarget() {
        // Init
        this.robotTargeting = new ArrayList<>();

        // Add classes
        this.robotTargeting.add("FIRST");
        this.robotTargeting.add("CLOSEST");
        this.robotTargeting.add("REVENGE");
        this.robotTargeting.add("SLAYER");
    }
}
