package Data;

import Comparators.MeasureEfficiencyComparator;
import java.util.ArrayList;
import java.util.Collections;

public class ClasseEfficiency {
    
    // ----- VARIABLES ----- //
        
        // Movement
        /* 
         * Still
         * Linear
         * Circle
         * Random
         */
        private ArrayList<Measure> robotMovement;
        
        // Shooting method
        /* 
         * Point
         * Linear
         * Circle
         * Random
         */
        private ArrayList<Measure> robotShooting;
        
        // Target choosing
        /* 
         * First
         * Closest
         * Revenge (Robot who did the most damage to you)
         * Slayer (Robot who has the lest knowned energy value)
         */
        private ArrayList<Measure> robotTargeting;
        
    // ----- CONSTRUCTORS ----- //
        
    public ClasseEfficiency() {
        this.constructMovement();
        this.constructShooting();
        this.constructTarget();
    }
    
    // ----- METHODS ----- //
    
    public ArrayList<Measure> getMovementMeasures() {
        return this.robotMovement;
    }
    
    public ArrayList<Measure> getShootingMeasures() {
        return this.robotShooting;
    }
    
    public ArrayList<Measure> getTargetingMeasures() {
        return this.robotTargeting;
    }
    
    public Measure getBestMovement() {
        return this.robotMovement.get(0);
    }
    
    public Measure getBestShooting() {
        return this.robotShooting.get(0);
    }
    
    public Measure getBestTargeting() {
        return this.robotTargeting.get(0);
    }
    
    public void missShot() {
        // Count received shot for current classes
        this.getBestMovement().missShot();
        this.getBestShooting().missShot();
        this.getBestTargeting().missShot();
        
        // Sort classes
        Collections.sort(this.robotMovement, new MeasureEfficiencyComparator());
        Collections.sort(this.robotShooting, new MeasureEfficiencyComparator());
        Collections.sort(this.robotTargeting, new MeasureEfficiencyComparator());
    }
    
    public void receiveShot() {
        // Count received shot for current classes
        this.getBestMovement().receiveShot();
        this.getBestShooting().receiveShot();
        this.getBestTargeting().receiveShot();
        
        // Sort classes
        Collections.sort(this.robotMovement, new MeasureEfficiencyComparator());
        Collections.sort(this.robotShooting, new MeasureEfficiencyComparator());
        Collections.sort(this.robotTargeting, new MeasureEfficiencyComparator());
    }
    
    public void shootSomeone() {
        // Count hit shot for current classes
        this.getBestMovement().shootSomeone();
        this.getBestShooting().shootSomeone();
        this.getBestTargeting().shootSomeone();
        
        // Sort classes
        Collections.sort(this.robotMovement, new MeasureEfficiencyComparator());
        Collections.sort(this.robotShooting, new MeasureEfficiencyComparator());
        Collections.sort(this.robotTargeting, new MeasureEfficiencyComparator());
    }
    
    // ----- AUXILIARY METHODS ---- //
    
    private void constructMovement() {
        // Init
        this.robotMovement = new ArrayList<>();
        
        // Create empty class measure
        Measure still = new Measure("STILL", 0);
        Measure linear = new Measure("LINEAR", 0);
        Measure circle = new Measure("CIRCLE", 0);
        Measure random = new Measure("RANDOM", 0);
        
        // Add classes
        this.robotMovement.add(still);
        this.robotMovement.add(linear);
        this.robotMovement.add(circle);
        this.robotMovement.add(random);
    }
    
    private void constructShooting() {
        // Init
        this.robotShooting = new ArrayList<>();
        
        // Create empty class measure
        Measure point = new Measure("POINT", 1);
        Measure linear = new Measure("LINEAR", 1);
        Measure circle = new Measure("CIRCLE", 1);
        Measure random = new Measure("RANDOM", 1);
        
        // Add classes
        this.robotShooting.add(point);
        this.robotShooting.add(linear);
        this.robotShooting.add(circle);
        this.robotShooting.add(random);
    }
    
    private void constructTarget() {
        // Init
        this.robotTargeting = new ArrayList<>();
        
        // Create empty class measure
        Measure first = new Measure("FIRST", 2);
        Measure closest = new Measure("CLOSEST", 2);
        Measure revenge = new Measure("REVENGE", 2);
        Measure slayer = new Measure("SLAYER", 2);
        
        // Add classes
        this.robotTargeting.add(first);
        this.robotTargeting.add(closest);
        this.robotTargeting.add(revenge);
        this.robotTargeting.add(slayer);
    }
}
