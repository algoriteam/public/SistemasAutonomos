package Team;

import Data.ClasseEfficiency;
import Data.Robot;
import Data.Measure;

import robocode.*;

import java.util.ArrayList;
import java.util.Random;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * AsylumRunner
 * 
 * TeamRobot that calculates most efficient class, this is, movement, shooting method and target choose. 
 * Additionally, tries to manage friendly fire, but doesn't communicate with its teammates
 *
 * @author AlgoriTeam
 */
public class AsylumRunner extends TeamRobot {
    
    // ----- VARIABLES ----- //
    
        // BATTLEGROUND
        
            private double battleWidth;
            private double battleHeight;
        
        // STATUS
            
            private Boolean isAlive;
        
        // POSITION
            
            private Point2D position;
            private ArrayList<Point2D> safePositions;
            
            // ----- Parameters -----
            private Integer safeNetDensity = 3; // 5
            private Double safeNetConeAngle = 60.0; // 180.0
        
        // CLASS
            
            private ClasseEfficiency robotClass;
            private String oldMovementClass;
            private String currentMovementClass;
            
            private String currentShootingMethod;
            private String currentTargetingMethod;
        
        // MOVEMENT
            
            private int currentRandomStep;
            
            // ----- Parameters -----
            private Double speed = 30.0; // 30.0

            private Double circleDegrees = 2.0; // 10.0
            private Integer randomTimeout = 30; // 10

            private Double wallSafeDistance = 50.0; // 50.0
            private Double wallTurningDegrees = 180.0; // 180.0
            private Double robotSafeDistance = this.wallSafeDistance + 2 * 25.0; // 100.0
        
        // DIRECTION + CONTROL
            
            private double directionAngle;
            private boolean directionSide;
        
        // HEADING
            
            private double bodyHeading;
            private double gunHeading;
        
        // TARGETING
            
            private String firstTarget;
            private String closestTarget;
            private String revengeTarget;
            private String slayerTarget;
            
            private Point2D shootingPoint;
            private LinkedHashMap<String, Robot> robots;
            
            // ----- Parameters -----
            private Double radarDegrees = 360.0; // 10.0
            private Double linearDeviation = 30.0; // 100.0
            private Double circleDeviationDegrees = 360.0; // 360.0
            
            private Double startShootingDistance = 1000.0; // 200.0
        
        // POWER
            
            private Double power = 2.5; // 1.0
            
        // FRIENDLY FIRE CONTROL
            
            // ----- Parameters -----
            private Double friendlyFireDeviationDegrees = 60.0; // 60.0
        
        // INFORMATION
            
            private Thread robotUpdater;
            private Thread targetUpdater;
            
            // ----- Parameters -----
            private Integer robotUpdateSpeed = 10; // 100
            private Integer targetUpdateSpeed = 10; // 100
        
        // DEBUG
        
            private Random randomPicker;
            
            // ----- Parameters -----
            private Boolean debugMode = true; // False
        
        
    // ----- RUN ----- //
    
    @Override
    public void run() {
        // Init values
        this.configureRobot();
        
        // Activate killing machine
        this.executeRobot();
    }
    
    // ----- EVENTS ----- //

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        Robot oldRobot;
        Robot newRobot;
        
        String robotName;
        Point2D robotPosition;
        double robotEnergy;
        double robotHeading;
        double robotVelocity;
        
        double angleToRobot;
        double realAngle;
            
        // Calculate enemy positions
        synchronized(this.robots) {
            // Add new position   
            angleToRobot = e.getBearing();

            // Calculate the angle to the scanned robot
            // The ScannedRobotEvent gives us a bearing to the scanned robot but it is relative to our tank's position, not our radar's position. How do we resolve this little quandry?
            // We find our heading (getHeading()) and add the bearing to the scanned robot (e.getBearing())
            realAngle = Math.toRadians((this.getHeading() + angleToRobot % 360));

            // Calculate enemy position
            robotPosition = new Point2D.Double((this.getX() + Math.sin(realAngle) * e.getDistance()), 
                                               (this.getY() + Math.cos(realAngle) * e.getDistance()));

            robotName = e.getName();
            robotEnergy = e.getEnergy();
            robotHeading = e.getHeading();
            robotVelocity = e.getVelocity();
            newRobot = new Robot(robotPosition.getX(),
                                 robotPosition.getY(),
                                 robotHeading,
                                 angleToRobot,
                                 robotVelocity,
                                 robotEnergy);

            // Check if robot info already exists
            if (!this.robots.containsKey(robotName))
                // Add recently found robot
                this.robots.put(robotName, newRobot);
            else {
                // Update already found robot
                oldRobot = (Robot) this.robots.get(e.getName());
                oldRobot.setPosition(robotPosition);
                oldRobot.setBodyHeading(robotHeading);
                oldRobot.setVelocity(robotVelocity);
                oldRobot.setRobotEnergy(robotEnergy);
            }
        }
    }

    @Override
    public void onRobotDeath(RobotDeathEvent e) {
        synchronized(this.robots) {
            // Clear enemy history and restart scan
            this.robots.clear();
        }
    }
    
    @Override
    public void onBulletMissed(BulletMissedEvent e){
        this.message("[DEBUG] Ohhh no, there goes a bullet ...");

        // Update classes
        this.robotClass.missShot();
        this.paintRobot(true);
    }

    @Override
    public void onBulletHit(BulletHitEvent e) {
        String robotName = e.getName();
        
        if(!this.isTeammate(robotName)) {
            this.message(String.format("[DEBUG] Muahahhaha, I shot '%s'!", robotName));

            // Update classes
            this.robotClass.shootSomeone();
            this.paintRobot(true);
        } else {
            this.message("[DEBUG] Oooppps, sorry mate, not on purpose!");
        }
    }
    
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        synchronized(this.robots) {
            String robotName = e.getName();
            
            if (!this.isTeammate(robotName)) {
                Robot enemy;
                double bulletPower = e.getBullet().getPower();
                double damage = bulletPower * 4;

                if(bulletPower > 1)
                    damage += 2 * (bulletPower - 1);

                this.message(String.format("[DEBUG] AhhhHHHHH, '%s' shot me!", robotName));

                // Is the enemy still alive????
                if (this.robots.containsKey(robotName)) {
                    // Update damage received from enemy
                    enemy = this.robots.get(robotName);
                    enemy.makeDamage(damage);

                    // Update classes
                    this.robotClass.receiveShot();
                    this.paintRobot(true);
                }
                else {
                    this.message("[DEBUG] Howw??? A ghost shot me!");
                }
            } else {
                this.message("[DEBUG] Watch out for the friendly fire!");
            }
        }
    }
    
    @Override
    public void onRoundEnded(RoundEndedEvent e) {
        this.isAlive = false;
    }
    
    @Override
    public void onDeath(DeathEvent e) {
        this.isAlive = false;
    }
    
    @Override
    public void onWin(WinEvent e) {
        while(true) {
            // Victoryyyyyyyy celebration
            this.turnRight(360);
            
            // Shoot for fun
            this.fire(this.power);
        }
    }
    
    @Override
    public void onHitWall(HitWallEvent e) {
        // At best, this method is never called by the robot! But if our safety hit check doesn't work, its here to help :)
        this.turnRight(this.wallTurningDegrees);
    }
    
    @Override
    public void onHitRobot(HitRobotEvent e) {
        // At best, this method is never called by the robot! But if our safety hit check doesn't work, its here to help :)
        this.turnRight(this.wallTurningDegrees);
    }
    
    // ----- METHODS ----- //
    
    private void configureRobot() {
        this.message("> [CONFIGURATION]");
                
        // BATTLEGROUND
        this.battleWidth = this.getBattleFieldWidth();
        this.battleHeight = this.getBattleFieldHeight();
        this.message(String.format("  Width: %.0f", this.battleWidth));
        this.message(String.format("  Height: %.0f", this.battleHeight));
        
        // STATUS
        this.isAlive = true;
        
        // POSITION
        this.position = new Point2D.Double();
        this.position.setLocation(this.getX(), this.getY());
        this.message(String.format("  Position: (%.2f, %.2f)", this.position.getX(), this.position.getY()));
        
        this.safePositions = new ArrayList<>();
        
        if (this.safeNetDensity == null)
            this.safeNetDensity = 5;
        
        if (this.safeNetConeAngle == null)
            this.safeNetConeAngle = 180.0;
        
        // CLASS
        this.robotClass = new ClasseEfficiency();
        this.oldMovementClass = "NONE";
        this.currentMovementClass = "NONE";
        
        this.currentShootingMethod = "NONE";
        this.currentTargetingMethod = "NONE";
        
        this.message("  Class:");
        this.message(String.format("    Movement: %s", this.robotClass.getBestMovement().getName()));
        this.message(String.format("    Shooting: %s", this.robotClass.getBestShooting().getName()));
        this.message(String.format("    Targeting: %s", this.robotClass.getBestTargeting().getName()));
        
        // MOVEMENT
        if (this.speed == null)
            this.speed = 30.0;
        
        if (this.circleDegrees == null)
            this.circleDegrees = 10.0;
        
        if (this.wallTurningDegrees == null)
            this.wallTurningDegrees = 180.0;
        
        if (this.randomTimeout == null)
            this.randomTimeout = 10;
        
        if (this.wallSafeDistance == null)
            this.wallSafeDistance = 50.0;
        
        if (this.robotSafeDistance == null)
            this.robotSafeDistance = 100.0;
        
        this.message("  Movement:");
        this.message(String.format("    Speed: %.2f", this.speed));
        this.message(String.format("    Circle turning degrees: %.2f", this.circleDegrees));
        this.message(String.format("    Hit turning degrees: %.2f", this.wallTurningDegrees));
        this.message(String.format("    Wall safe distance: %.2f", this.wallSafeDistance));
        this.message(String.format("    Robot safe distance: %.2f", this.robotSafeDistance));
        this.message(String.format("    Random timeout: %d", this.randomTimeout));
        
        // DIRECTION + CONTROL
        this.directionAngle = 0.0;
        this.directionSide = true;
        this.message("  Direction:");
        this.message(String.format("    Side: %s", this.directionSide ? "Right" : "Left"));
        this.message(String.format("    Angle: %.2f", this.directionAngle));
        
        // HEADING
        this.bodyHeading = this.getHeading();
        this.gunHeading = this.getGunHeading();
        this.message("  Heading:");
        this.message(String.format("    Body: %.2f", this.bodyHeading));
        this.message(String.format("    Gun: %.2f", this.gunHeading));
        
        // TARGETING
        this.firstTarget = "NONE";
        this.closestTarget = "NONE";
        this.revengeTarget = "NONE";
        this.slayerTarget = "NONE";
        this.shootingPoint = new Point2D.Double();
        this.robots = new LinkedHashMap<>();
        
        if (this.radarDegrees == null)
            this.radarDegrees = 10.0;
        
        if (this.linearDeviation == null)
            this.linearDeviation = 100.0;
        
        if (this.circleDeviationDegrees == null)
            this.circleDeviationDegrees = 360.0;
        
        if (this.startShootingDistance == null)
            this.startShootingDistance = 200.0;
            
        this.message("  Radar:");
        this.message(String.format("    Turning degrees: %.2f", this.radarDegrees));
        this.message(String.format("    Linear deviation: %.2f", this.linearDeviation));
        this.message(String.format("    Circle deviation degrees: %.2f", this.circleDeviationDegrees));
        
        // POWER
        if (this.power == null)
            this.power = 1.0;
        this.message(String.format("  Power: %.0f", this.power));
        
        // FRIENDLY FIRE
        if (this.friendlyFireDeviationDegrees == null)
            this.friendlyFireDeviationDegrees = 60.0;
        this.message(String.format("  Friendly fire safe zone: %.0f", this.friendlyFireDeviationDegrees));
        
        // INFORMATION
        this.robotUpdater = new Thread(new UpdateInfo());
        this.robotUpdater.start();
        
        this.targetUpdater = new Thread(new UpdateTarget());
        this.targetUpdater.start();
        
        if (this.robotUpdateSpeed == null) {
            this.robotUpdateSpeed = 100;
        }
        
        if (this.targetUpdateSpeed == null) {
            this.targetUpdateSpeed = 100;
        }
        
        // DEBUG
        this.randomPicker = new Random();
        
        // Other adjustments
        this.setAdjustRadarForRobotTurn(true);
        this.setAdjustRadarForGunTurn(true);
        this.setAdjustGunForRobotTurn(true);
        
        // Paint robot
        this.paintRobot(true);
    }
    
    private void executeRobot() {
        while (true) {
            // Scan
            this.setTurnRadarRight(this.radarDegrees);
            
            // Gun
            if(this.shootingPoint.getX() > 0 && this.shootingPoint.getY() > 0 && this.position.distance(this.shootingPoint) < this.startShootingDistance) {
            
                // Turn weapon to target
                this.turnGunToTarget(this.shootingPoint);

                // Check for friendly fire
                if (!this.isGoingToFriendlyFire())
                    // Fire weapon
                    this.fire(this.power);
            
            }
            
            // Movement
            this.oldMovementClass = this.robotClass.getBestMovement().getName();
            
            // Movement type changed
            if (!this.oldMovementClass.equals(this.currentMovementClass)) {
                this.currentMovementClass = this.oldMovementClass;
                this.newMovement();
            } 
            // Keep current movement
            else this.continueMovement();
            
            // Force movement
            this.execute();
            
            // Force scan
            this.scan();
            
            // Force paint
            this.paintRobot(false);
        }
    }
    
    // ----- AUXILIARY METHODS ----- //
    
    // MOVEMENT
    private void newMovement() {
        switch(this.currentMovementClass) {
            case "STILL":
                // Nothing to do here
                this.setAhead(0);
                break;
            case "LINEAR":
                this.chooseDirection();
                break;
            case "CIRCLE":
                this.chooseDirection();
                break;
            case "RANDOM":
                this.chooseDirection();
                this.currentRandomStep = 0;
                break;
        }
    }
    
    private void continueMovement() {
        Point2D closestTank;
        
        // Check if robot is going to hit a wall within the safe distance
        if (this.isGoingToHitWall())
            this.turnBodyToTarget(new Point2D.Double(this.battleWidth * 0.5, this.battleHeight * 0.5), false);
        
        // Check if robot is going to hit someone within the safe distance
        closestTank = this.isGoingToHitRobot();
        if (closestTank != null)
            // Turn to target inverse (NOTE: Remember that this coordinate system only handles with positive values!)
            this.turnBodyToTarget(new Point2D.Double(this.position.getX() - (closestTank.getX() - this.position.getX()), 
                                                     this.position.getY() - (closestTank.getY() - this.position.getY())),
                                                     false);
        
        // Keep on moving
        switch(this.currentMovementClass) {
            case "STILL":
                // Nothing to do here
                this.setAhead(0);
                break;
            case "LINEAR":
                this.setAhead(this.speed);
                break;
            case "CIRCLE":
                this.setTurnRight(this.circleDegrees);
                this.setAhead(this.speed);
                break;
            case "RANDOM":
                if (this.currentRandomStep < this.randomTimeout) {
                    this.setAhead(this.speed);
                    this.currentRandomStep++;
                } else {
                    this.currentRandomStep = 0;
                    this.chooseDirection();
                }
                break;
        }
    }
    
    private void chooseDirection() {
        this.directionSide = this.randomPicker.nextInt(100) > 50;
        this.directionAngle = this.randomPicker.nextDouble() * 180.0;
        
        if(this.directionSide) {
            this.setTurnRight(this.directionAngle);
        } 
        else this.setTurnLeft(this.directionAngle);
    }
    
    private void turnBodyToTarget(Point2D t, boolean set) {
        double angle = this.normalRelativeAngle(this.absoluteBearing(this.position, t) - this.bodyHeading);
        if(set)
            this.setTurnRight(angle);
        else this.turnRight(angle);
    }
    
    private boolean isGoingToHitWall() {
        synchronized(this.safePositions){
            boolean result = false;
            
            for(Point2D pos: this.safePositions) {
                if (pos.getX() < 0.0 || pos.getX() > this.battleWidth || pos.getY() < 0.0 || pos.getY() > this.battleHeight) {
                    result = true;
                    break;
                }
            }
            
            return result;
        }
    }
    
    private Point2D isGoingToHitRobot() {
        synchronized(this.robots){
            Point2D result = null;
            Iterator<String> it = this.robots.keySet().iterator();
            
            String currentName;
            Robot currentRobot;
            Point2D currentPosition;
            double minDistance = Double.MAX_VALUE;
            double currentDistance;
            
            // Check robots
            while(it.hasNext()) {
                currentName = it.next();
                currentRobot = this.robots.get(currentName);
                currentPosition = currentRobot.getPosition();
                currentDistance = this.position.distance(currentPosition);
                if (currentDistance < (this.robotSafeDistance + this.speed) && (result == null || currentDistance < minDistance)) {
                    minDistance = currentDistance;
                    result = currentPosition;
                }
            }

            return result;
        }
    }
    
    private boolean isGoingToFriendlyFire() {
        synchronized(this.robots){
            synchronized(this.shootingPoint) {
                boolean result = false;
                Iterator<String> it = this.robots.keySet().iterator();

                String currentName;
                Robot currentRobot;
                Point2D currentPosition;

                double distanceTeammate;
                double angleTeammate;
                double safeAngle = this.friendlyFireDeviationDegrees - this.friendlyFireDeviationDegrees * 0.5;
                double distanceShootingPoint = this.position.distance(this.shootingPoint);
                
                // Check if any teammates are within your range
                while(it.hasNext()) {
                    currentName = it.next();
                    currentRobot = this.robots.get(currentName);
                    currentPosition = currentRobot.getPosition();
                    distanceTeammate = this.position.distance(currentPosition);
                    angleTeammate = this.normalRelativeAngle(this.absoluteBearing(this.position, currentPosition) - this.gunHeading);
                    if (this.isTeammate(currentName) && distanceTeammate < distanceShootingPoint && angleTeammate > -safeAngle && angleTeammate < safeAngle) {
                        result = true;
                        break;
                    }
                }

                return result;
            }
        }
    }
    
    // TARGETING
    private String getCurrentTarget() {
        String result = "NONE";
        
        this.currentTargetingMethod = this.robotClass.getBestTargeting().getName();
        switch(this.currentTargetingMethod) {
            case "FIRST":
                result = this.firstTarget;
                break;
            case "CLOSEST":
                result = this.closestTarget;
                break;
            case "REVENGE":
                result = this.revengeTarget;
                break;
            case "SLAYER":
                result = this.slayerTarget;
                break;
        }
        
        return result;
    }
    
    private void turnGunToTarget(Point2D t) {
        double angle = this.normalRelativeAngle(this.absoluteBearing(this.position, t) - this.gunHeading);
        this.turnGunRight(angle);
    }
    
    // ----- CONSOLE METHODS ----- //
    
    private void message(String m) {
        System.out.println(m);
    }
    
    private String prettyArray(ArrayList<Measure> a) {
        StringBuilder sb = new StringBuilder();
        
        sb.append("[ ");
        for(Measure m : a){
            sb.append(String.format("%.2f (%s) ", m.getEfficiency(), m.getName()));
        }
        sb.append("]");
        
        return sb.toString();
    }
    
    // ----- DEBUG PAINT ----- //
    
    private void paintRobot(boolean debug) {
        Color color = Color.BLACK;
        String movement = this.robotClass.getBestMovement().getName();
        String shooting = this.robotClass.getBestShooting().getName();
        String targeting = this.robotClass.getBestTargeting().getName();
        
        if (debug) {
            this.message("> [REPAINT]");

            // Class
            this.message("  Class:");
            this.message(String.format("    Movement: %s -> %s", this.prettyArray(this.robotClass.getMovementMeasures()), movement));
            this.message(String.format("    Shooting: %s -> %s", this.prettyArray(this.robotClass.getShootingMeasures()), shooting));
            this.message(String.format("    Targeting: %s -> %s", this.prettyArray(this.robotClass.getTargetingMeasures()), targeting));
        }
        
        // Movement
        switch(movement) {
            case "STILL":
                color = Color.RED;
                break;
            case "LINEAR":
                color = Color.GREEN;
                break;
            case "HALFCIRCLE":
                color = Color.BLUE;
                break;
            case "CIRCLE":
                color = Color.YELLOW;
                break;
            case "RANDOM":
                color = Color.WHITE;
                break;
        }
        this.setBodyColor(color);
        
        // Shooting
        switch(shooting) {
            case "POINT":
                color = Color.RED;
                break;
            case "LINEAR":
                color = Color.GREEN;
                break;
            case "CIRCLE":
                color = Color.BLUE;
                break;
            case "RANDOM":
                color = Color.YELLOW;
                break;
        }
        this.setGunColor(color);
        
        // Target
        switch(targeting) {
            case "FIRST":
                color = Color.RED;
                break;
            case "CLOSEST":
                color = Color.GREEN;
                break;
            case "REVENGE":
                color = Color.BLUE;
                break;
            case "SLAYER":
                color = Color.YELLOW;
                break;
        }
        this.setRadarColor(color);
    }
    
    @Override
    public void onPaint(Graphics2D g) {
        if (this.debugMode) {
            // Set the paint color to blue
            g.setColor(Color.BLUE);

            // Position
            g.fillOval((int) this.position.getX() - 8, (int) this.position.getY() - 8, 16, 16);
                        
            // Robots
            synchronized(this.robots) {
                Iterator<String> it = this.robots.keySet().iterator();
               
                String robotName;
                Robot robot;
                Point2D robotPosition;
                
                while(it.hasNext()) {
                    robotName = it.next();
                    robot = this.robots.get(robotName);
                    robotPosition = robot.getPosition();
                    
                    if(this.isTeammate(robotName))
                        // Set the paint color to blue
                        g.setColor(new Color(0, 0, 1.0f, 0.5f));
                    else
                        // Set the paint color to red
                        g.setColor(new Color(1.0f, 0, 0, 0.5f));
                    
                    g.fillRect((int) robotPosition.getX() - 20, (int) robotPosition.getY() - 20, 40, 40);
                }
            }
            
            // Set the paint color to pink
            g.setColor(Color.PINK);
            
            // Safe positions
            synchronized(this.safePositions) {
                for(Point2D pos : this.safePositions) {
                    g.fillOval((int) pos.getX() - 5, (int) pos.getY() - 5, 10, 10);
                }
            }
            
            // Set the paint color to white
            g.setColor(Color.WHITE);
            
            // Shooting point
            if (this.shootingPoint != null) {
                g.drawLine((int) this.position.getX(), (int) this.position.getY(), (int) this.shootingPoint.getX(), (int) this.shootingPoint.getY());
                g.fillOval((int) this.shootingPoint.getX() - 5, (int) this.shootingPoint.getY() - 5, 10, 10);
            }
        }
    }
    
    // ----- AUXILIARY METHODS ----- //
    
    private double absoluteBearing(Point2D source, Point2D target) {
        return Math.toDegrees(Math.atan2(target.getX() - source.getX(), target.getY() - source.getY()));
    }
    
    private double normalRelativeAngle(double angle) {
       double relativeAngle = angle % 360;
       if (relativeAngle <= -180)
           return 180 + (relativeAngle % 180);
       else if (relativeAngle > 180)
           return -180 + (relativeAngle % 180);
       else
           return relativeAngle;
    }
    
    // ----- INFORMATION THREAD ----- //
    
    // INFORMATION UPDATE
    private void updateRobot() {
        // Position
        this.position.setLocation(this.getX(), this.getY());
        
        // Heading
        this.bodyHeading = this.getHeading();
        this.gunHeading = this.getGunHeading();
        
        double correctAngle = -(this.bodyHeading - 90 - this.safeNetConeAngle * 0.5);
        
        double currentRadians;
        double jump = this.safeNetConeAngle / (this.safeNetDensity - 1);
        
        double vX;
        double vY;
        
        synchronized(this.safePositions) {
            // Clear list
            this.safePositions.clear();
            
            // Calculate new points
            for(int i = 0; i < this.safeNetDensity; i++){
                currentRadians = Math.toRadians(correctAngle - i * jump);
                vX = this.position.getX() + Math.cos(currentRadians) * (this.wallSafeDistance + this.speed);
                vY = this.position.getY() + Math.sin(currentRadians) * (this.wallSafeDistance + this.speed);

                this.safePositions.add(new Point2D.Double(vX, vY));
            }
        }
    }
    
    private void updateTargets() {
        Iterator<String> it = this.robots.keySet().iterator();

        String currentName;
        Robot currentRobot;

        // FIRST
        String tempFirst = "NONE";
        while(it.hasNext()) {
            currentName = it.next();
            if (!this.isTeammate(currentName)) {
                tempFirst = currentName;
                break;
            }
        }
        this.firstTarget = tempFirst;

        // CLOSEST
        String closestResult = "NONE";
        Point2D currentPosition;
        double minDistance = Double.MAX_VALUE;
        double currentDistance;

        // REVENGE
        String revengeResult = "NONE";
        double maxDamage = Double.MIN_VALUE;
        double currentDamage;

        // SLAYER
        String slayerResult = "NONE";
        double minLife = Double.MAX_VALUE;
        double currentLife;
        
        it = this.robots.keySet().iterator();
        while(it.hasNext()) {
            currentName = it.next();
            currentRobot = this.robots.get(currentName);
            
            if (!this.isTeammate(currentName)) {
                // CLOSEST
                currentPosition = currentRobot.getPosition();
                currentDistance = this.position.distance(currentPosition);
                if (closestResult.equals("NONE") || currentDistance < minDistance) {
                    minDistance = currentDistance;
                    closestResult = currentName;
                }

                // REVENGE
                currentDamage = currentRobot.getStolenEnergy();
                if (revengeResult.equals("NONE") || currentDamage > maxDamage) {
                    maxDamage = currentDamage;
                    revengeResult = currentName;
                }

                // SLAYER
                currentLife = currentRobot.getRobotEnergy();
                if (slayerResult.equals("NONE") || currentLife < minLife) {
                    minLife = currentLife;
                    slayerResult = currentName;
                }
            }
        }

        // Set new values
        this.closestTarget = closestResult;
        this.revengeTarget = revengeResult;
        this.slayerTarget = slayerResult;
    }
    
    private void updateShootingPoint() {
        String enemyName = this.getCurrentTarget();
        Robot enemy;
        Point2D enemyPosition;
        double enemyHeading;
        double enemyVelocity;

        Point2D tempShootingPoint = new Point2D.Double();

        // LINEAR AND CIRCLE
        double correctRadians;

        if (this.robots.containsKey(enemyName)) {
            enemy = this.robots.get(enemyName);
            enemyPosition = enemy.getPosition();
            enemyHeading = enemy.getBodyHeading();
            enemyVelocity = enemy.getVelocity() + this.linearDeviation;

            this.currentShootingMethod = this.robotClass.getBestShooting().getName();
            switch(this.currentShootingMethod){
                case "POINT":
                    tempShootingPoint = enemyPosition;
                    break;
                case "LINEAR":
                    correctRadians = Math.toRadians(-(enemyHeading - 90));
                    tempShootingPoint.setLocation(enemyPosition.getX() + Math.cos(correctRadians) * enemyVelocity,
                                                   enemyPosition.getY() + Math.sin(correctRadians) * enemyVelocity);
                    break;
                case "CIRCLE":
                    correctRadians = Math.toRadians(-(enemyHeading - 90) + this.randomPicker.nextDouble() * this.circleDeviationDegrees - this.circleDeviationDegrees * 0.5);
                    tempShootingPoint.setLocation(enemyPosition.getX() + Math.cos(correctRadians) * enemyVelocity,
                                                   enemyPosition.getY() + Math.sin(correctRadians) * enemyVelocity);
                    break;
                case "RANDOM":
                    tempShootingPoint.setLocation(enemyPosition.getX() + (2 * this.randomPicker.nextDouble() * enemyVelocity) - enemyVelocity, 
                                                   enemyPosition.getY() + (2 * this.randomPicker.nextDouble() * enemyVelocity) - enemyVelocity);
                    break;
            }

            this.shootingPoint = tempShootingPoint;
        }
    }
    
    private class UpdateInfo implements Runnable {
        
        @Override
        public void run() {
            while(true) {
                if (isAlive) {
                    // Update robot info
                    updateRobot();
                } 
                else break;
                
                try {
                    Thread.sleep(robotUpdateSpeed);
                } catch (InterruptedException ex) {
                    message("[DEBUG] Error in update thread sleep");
                }
            }
        }
        
    }
    
    private class UpdateTarget implements Runnable {
        
        @Override
        public void run() {
            while(true) {
                if (isAlive) {
                    synchronized(robots) {
                        if (robots.size() > 0) {
                            // Update target and shooting point info
                            updateTargets();
                            updateShootingPoint();
                        }
                    }
                } 
                else break;
                
                try {
                    Thread.sleep(targetUpdateSpeed);
                } catch (InterruptedException ex) {
                    message("[DEBUG] Error in update thread sleep");
                }
            }
        }
        
    }
}
