/**
 * Copyright (c) 2001-2017 Mathew A. Nelson and Robocode contributors
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://robocode.sourceforge.net/license/epl-v10.html
 */
package Examples;

import Tools.DistanceThread;
import java.text.DecimalFormat;
import robocode.*;

/**
 * MyFirstRobot - a sample robot by Mathew Nelson.
 * 
 * Moves in a seesaw motion, and spins the gun around at each end.
 *
 * @author Mathew A. Nelson (original)
 */
public class DistanceRobot_Thread extends Robot {

    private DistanceThread counter;
    
    /**
     * MyFirstRobot's run method - Seesaw
     */
    @Override
    public void run() {
        
        this.counter = new DistanceThread(this);
        this.counter.start();
        
        while (true) {
            ahead(100); // Move ahead 100
            turnGunRight(360); // Spin gun around
            back(100); // Move back 100
            turnGunRight(360); // Spin gun around
        }
    }

    /**
     * Fire when we see a robot
     * 
     * @param e
     */
    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        fire(1);
    }

    /**
     * We were hit!  Turn perpendicular to the bullet,
     * so our seesaw might avoid a future shot.
     * 
     * @param e
     */
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        turnLeft(90 - e.getBearing());
    }
    
    // ----- END EVENTS ----- //

    /**
     * Event called on round end
     *
     * @param event
     */
    @Override
    public void onRoundEnded(RoundEndedEvent event) {
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("[ROUND " + (this.getRoundNum() + 1) + "] Distance: " + df.format(counter.getDistance()) + " pixels");
        counter.kill();
    }
}												
