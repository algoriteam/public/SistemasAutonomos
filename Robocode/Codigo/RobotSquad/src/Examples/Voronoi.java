/******************************************************************************
 *  Compilation:  javac Voronoi.java
 *  Execution:    java Voronoi
 *  Dependencies: Draw.java DrawListener.java
 * 
 *  Plots the points that the user clicks, and draws the Voronoi diagram.
 *  We assume the points lie on an M-by-M grid and use a brute force
 *  discretized algorithm. Each insertion takes time proportional to M^2.
 *
 *  Limitations
 *  -----------
 *    - Running time scales (badly) with M
 *    - Fortune's algorithm can compute a Voronoi diagram on N 
 *      points in time proportional to N log N, but it is 
 *      subtantially more complicated than this program which is intended
 *      to demonstrate callbacks and GUI operations.
 *
 ******************************************************************************/

package Examples;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
 
import javax.imageio.ImageIO;
import javax.swing.JFrame;
 
public class Voronoi extends JFrame {
    static BufferedImage I;
    static ArrayList<Point2D> points;
    static ArrayList<Color> colors;
    static ArrayList<Point2D> draw;
    static int cells = 6, size = 1000;

    public Voronoi() {
        super("Voronoi Diagram");
        setBounds(0, 0, size, size);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        int oldN = 0;
        int n = 0;
        Random rand = new Random();
        I = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        points = new ArrayList<>();
        colors = new ArrayList<>();
        draw = new ArrayList<>();

        Point2D pixelPoint = new Point2D.Double();
        Point2D oldPoint = new Point2D.Double(5000, 5000);
        Point2D newPoint = new Point2D.Double(5000, 5000);
        
        // Init points and colors
        for (int i = 0; i < cells; i++) {
            points.add(new Point2D.Double(rand.nextInt(size), rand.nextInt(size)));
            colors.add(new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255)));
        }
        
        boolean first = true;
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                pixelPoint.setLocation(x, y);
                first = true;
                
                for(Point2D p : points) {
                    if (first) {
                        newPoint.setLocation(p);
                        first = false;
                    }
                    
                    if (p.distance(pixelPoint) < newPoint.distance(pixelPoint)) {
                        newPoint.setLocation(p);
                    }
                }

                if (!oldPoint.equals(newPoint)){
                    oldPoint.setLocation(newPoint);
                    draw.add(new Point2D.Double(x, y));
                }
            }
        }


        Graphics2D g = I.createGraphics();
        g.setColor(Color.PINK);
        for (Point2D p : points) {
            g.fill(new Ellipse2D.Double(p.getX() - 5, p.getY() - 5, 10, 10));
        }
        
        g.setColor(Color.WHITE);
        for (Point2D p : draw) {
            g.fill(new Ellipse2D.Double(p.getX() - 1, p.getY() - 1, 2, 2));
        }

        try {
            ImageIO.write(I, "png", new File("voronoi.png"));
        } catch (IOException e) {}

    }
    
    static double distance(int x1, int x2, int y1, int y2) {
            double d;
            d = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); // Euclidian
            //  d = Math.abs(x1 - x2) + Math.abs(y1 - y2); // Manhattan
            //  d = Math.pow(Math.pow(Math.abs(x1 - x2), p) + Math.pow(Math.abs(y1 - y2), p), (1 / p)); // Minkovski
            return d;
    }

    public void paint(Graphics g) {
        g.drawImage(I, 0, 0, this);
    }

    public static void main(String[] args) {
        new Voronoi().setVisible(true);
    }
}
